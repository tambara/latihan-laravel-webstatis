<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="welcome" method="POST">
        @csrf
        <label for="firstname">First name:</label><br><br>
        <input type="text" id="firstname" name="fname">

        <br><br>
        
        <label for="lastname">Last name:</label><br><br>
        <input type="text" id="lastname" name="lname">

        <br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="M">Male<br>
        <input type="radio" name="gender" value="F">Female<br>
        <input type="radio" name="gender" value="O">Other

        <br><br>

        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="ind">Indonesian</option>
            <option value="rus">Russia</option>
            <option value="bra">Brazil</option>
            <option value="spa">Spanyol</option>
        </select>

        <br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="lang" value="indo">Bahasa Indonesia<br>
        <input type="checkbox" name="lang" value="eng">English<br>
        <input type="checkbox" name="lang" value="oth">Other

        <br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea cols="30" rows="10" id="bio"></textarea>

        <br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>
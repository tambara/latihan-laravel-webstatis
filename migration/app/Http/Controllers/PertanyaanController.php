<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
/* panggil db*/
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
        // mengecek output user yg akan masuk database
        // dd($request->all());

        // memvalidasi data yg dimasukkan user
        $request->validate([
            'judul' => 'required',
            'pertanyaan' => 'required'
        ]);

        $query = DB::table('pertanyaan')->insert([
            'judul' => $request['judul'],
            'isi' => $request['pertanyaan'],
            'tanggal_dibuat' => date('Y-m-d H:i:s'),
            'tanggal_diperbaharui' => date('Y-m-d H:i:s')
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Disimpan');
    }

    public function index(){
        $post = DB::table('pertanyaan')->get();
        return view('pertanyaan.index', compact('post'));
    }

    public function show($id){
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id){
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required',
            'pertanyaan' => 'required'
        ]);

        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'isi' => $request['pertanyaan']
                    ]);
        return redirect('/pertanyaan')->with('success', 'Berhasil Ubah Pertanyaan');
    }

    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dihapus');
    }
}

@extends('adminlte.master')

@section('content')
    <div class="ml-4 mt-3 mr-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Ubah Pertanyaan {{$pertanyaan->id}}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                @csrf
                @method("PUT")
                <div class="card-body">
                    <div class="form-group">
                        <label form="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul', $pertanyaan->judul)}}" placeholder="Masukkan Judul">
                        @error("judul")
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label form="pertanyaan">Pertanyaan</label>
                        <input type="text" class="form-control" id="pertanyaan" name="pertanyaan" value="{{old('isi', $pertanyaan->isi)}}" placeholder="Masukkan Pertanyaan">
                        @error("pertanyaan")
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
            <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
    </div>
@endsection
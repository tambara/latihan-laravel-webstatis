@extends('adminlte.master')

@section('content')
    <div class="ml-4 mt-3 mr-4">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Buat Pertanyaan</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="/pertanyaan" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label form="judul">Judul</label>
                        <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul', '')}}" placeholder="Masukkan Judul">
                        @error("judul")
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label form="pertanyaan">Pertanyaan</label>
                        <input type="text" class="form-control" id="pertanyaan" name="pertanyaan" placeholder="Masukkan Pertanyaan">
                        @error("pertanyaan")
                            <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                    </div>
                </div>
            <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Buat</button>
                </div>
            </form>
        </div>
    </div>
@endsection